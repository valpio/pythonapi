#!flask/bin/python
from flask import Flask, request
from GetRecord import GetRecord
app = Flask(__name__)

@app.route('/index.html')
def index():
    return "<p1>Hello, World!</p1> <b>this is inndex.html</b> "

@app.route('/params')
def params():
    arg1 = request.args['code']
    arg2 = request.args['date']
    output = GetRecord.getData('GetRecord', arg1, arg2)

    present  ='<div>' + 'code =' +output['code'].iloc[0]+'</div>'
    present +='<div>' + ' open ='+ str(output['market_open'].iloc[0])+'</div>'
    present +='<div>' + ' close =' + str(output['market_price'].iloc[0])+'</div>'
    return   present

if __name__ == '__main__':
    app.run(debug=True)
