import pandas as pd

class GetRecord:
    def getData(self, code, date):
        data = pd.read_csv("/home/coder/SampleData/finance_0803_0828.csv")
        data['sys_load_date'] = data['sys_load_time'].str.slice(0,10)
        filterCode = data[data['code'] == code]
        filterDate = filterCode[filterCode['sys_load_date'] == date]

        return filterDate